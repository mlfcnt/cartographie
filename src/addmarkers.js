function addMarkers(data, placeholder) {
    // reset();
    for (var i = 0; i < data.records.length; i++) {
      if (data.records[i].fields.localisation === undefined) {
        continue;
      }
      if (data.records[i].fields.rubrique === placeholder) {
  
        jsonLength.push(data.records[i].fields);
        var name = (data.records[i].fields.nom);
        console.log(data.records[0].fields.nom)
        if (name === undefined) {
          name = "Non renseigné";
        };
  
        var long = (data.records[i].fields.localisation[1]);
        var lat = (data.records[i].fields.localisation[0]);
        var adr1 = (data.records[i].fields.adr1);
        if (adr1 === undefined) {
          adr1 = " ";
        };
        var adr2 = (data.records[i].fields.adr2);
        if (adr2 === undefined) {
          adr2 = " ";
        };
        var adr3 = (data.records[i].fields.adr3);
        if (adr3 === undefined) {
          adr3 = " ";
        };
        var objet = (data.records[i].fields.objet);
        if (objet === undefined) {
          objet = "Non renseigné";
        };
        var rubrique = (data.records[i].fields.rubrique);
        if (rubrique === undefined) {
          rubrique = "Aucune";
        };
        var sousrubrique = (data.records[i].fields.sousrubrique);
        if (sousrubrique === undefined) {
          sousrubrique = "Aucune";
        };
        var nompres = (data.records[i].fields.nompres);
        if (nompres === undefined) {
          nompres = "Non renseigné";
        };
        var telpres = (data.records[i].fields.telpres);
        if (telpres === undefined) {
          telpres = "Non renseigné";
        };
        var mail = (data.records[i].fields.mail);
        if (mail === undefined) {
          mail = "Non renseigné";
        };
        var web = (data.records[i].fields.web);
        if (web === undefined) {
          web = "Non renseigné";
        };
        var marker = L.marker([lat, long]).addTo(layerGroup);
        marker.bindPopup(' <br> Nom : ' + titleCase(name) + '.' + '<br>Adresse : ' + titleCase(adr1) + ' ' + titleCase(adr2) + ' ' + titleCase(adr3) + '<br> Rubrique : ' + titleCase(rubrique) + '<br> Sous-rubrique : ' + titleCase(sousrubrique) + '<br> Président(e) : ' + nompres + '<br> Téléhone du/de la Président(e) : ' + telpres + '<br>Mail : ' + mail + '<br> Web : ' + web);
  
        generateList(name, adr1, adr2, adr3, rubrique, sousrubrique, objet, nompres, telpres, mail, web)
  
      }
  
    }
  };

  export {addMarkers};